# Ansible

This is used for configuring our appliances that aren't well supported directly in terraform. For instance, all of our mikrotik routers. It is primarily intended to be run via CICD.

## CI/CD Vars

The following environment vars are assumed to be present if executing this ansible configuration.

- romon_secret
- wireguard_private_key
- wireguard_public_key
- ospf_secret
- env
- TF_VAR_ansible_ssh_secret
- CI_API_V4_URL
- CI_PROJECT_ID
- TF_VAR_gitlab_access_token
- bgp_secret
- er_ovpn_ca_key_passphrase
- er_ovpn_ca_key
- er_ppp_users
- hamwan_ssh_pass_22

For local execution, it is suggested to create a local "bootstrap.sh" file that exports these vars with their values.

## Running

```
source bootstrap.sh
ansible-playbook -i ../inventory.yaml export_router_configs.yaml
ansible-playbook -i ../inventory.yaml linux_setup.yaml --limit allstar.leb
```
