# APRS_IGATE Role

## Assumptions

- Raspberry PI 32
- PITNC or http://www.wvcarc.com/p/tnc-96k-resources.html
- http://www.cantab.net/users/john.wiseman/Downloads/Beta/TeensyProjects.zip

## Monitoring

```
node_systemd_unit_state{name="aprx.service",state="active",type="simple"}
```
